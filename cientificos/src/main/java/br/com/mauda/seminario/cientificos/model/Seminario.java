package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seminario implements Serializable {

    private static final long serialVersionUID = 997084310847650620L;

    private Long id;
    private String titulo;
    private String desricao;
    private Boolean mesaRedonda;
    private Date data;
    private Integer qtdInscricoes;
    private List<AreaCientifica> areasCientificas = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();

    public Seminario(String titulo, String desricao, Boolean mesaRedonda, Date data, Integer qtdInscricoes, AreaCientifica areaCientifica,
        List<Professor> professores, List<AreaCientifica> areasCientificas) {
        super();
        this.titulo = titulo;
        this.desricao = desricao;
        this.mesaRedonda = mesaRedonda;
        this.data = data;
        this.qtdInscricoes = qtdInscricoes;
        this.areasCientificas = areasCientificas;
        this.professores = professores;
        for (int i = 0; i < qtdInscricoes; i++) {
            this.inscricoes.add(new Inscricao(this));
        }
    }

    public Long getId() {
        return this.id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDesricao() {
        return this.desricao;
    }

    public void setDesricao(String desricao) {
        this.desricao = desricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public Date getData() {
        return this.data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Seminario other = (Seminario) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
}