package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Professor implements Serializable {

    private static final long serialVersionUID = 997084310847650620L;

    private Long id;
    private String nome;
    private Double salario;
    private String email;
    private Instituicao instituicao;
    private List<Seminario> seminarios = new ArrayList<>();

  

    public Professor(String nome, Double salario, String email, Instituicao instituicao) {
        super();
        this.nome = nome;
        this.salario = salario;
        this.email = email;
        this.instituicao = instituicao;
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSalario() {
        return this.salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public void adicionarSeminario(Seminario seminario) {
        this.seminarios.add(seminario);
    }

    public boolean possuiSeminario(Seminario seminario) {
        return this.seminarios.contains(seminario);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Professor other = (Professor) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

}