package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao implements Serializable {

    private static final long serialVersionUID = 997084310847650620L;

    private Long id;
    private Seminario seminario;
    private Estudante estudante;
    private boolean direitoMaterial;
    private SituacaoInscricaoEnum situacaoInscricaoEnum;

    public Inscricao(Seminario seminario) {
        super();
        this.seminario = seminario;
        this.situacaoInscricaoEnum = SituacaoInscricaoEnum.DISPONIVEL;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public boolean isDireitoMaterial() {
        return this.direitoMaterial;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }


    public SituacaoInscricaoEnum getSituacaoInscricaoEnum() {
        return this.situacaoInscricaoEnum;
    }

    public void setSituacaoInscricaoEnum(SituacaoInscricaoEnum situacaoInscricaoEnum) {
        this.situacaoInscricaoEnum = situacaoInscricaoEnum;
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.estudante = estudante;
        this.estudante.adicionarIncricao(this);
        this.situacaoInscricaoEnum = SituacaoInscricaoEnum.COMPRADO;
        this.direitoMaterial = direitoMaterial;
    }

    public void cancelarCompra() {
        if (this.situacaoInscricaoEnum.equals(SituacaoInscricaoEnum.COMPRADO)) {
            this.situacaoInscricaoEnum = SituacaoInscricaoEnum.DISPONIVEL;
        }
    }

    public void realizarCheckIn() {
        if (this.situacaoInscricaoEnum.equals(SituacaoInscricaoEnum.COMPRADO)) {
            this.situacaoInscricaoEnum = SituacaoInscricaoEnum.CHECKIN;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Inscricao other = (Inscricao) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

}